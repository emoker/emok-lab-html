/**
 * emokr-toast
 */
 function Toast() {

 }
 Toast.prototype = {
   ele:'',
   eleHtml:'<div id="toast" class="toast"><div class="toast-icon "></div><div class="toast-text"></div></div>',
   icon:'none',
   text:'默认提示',
   duration:2000,
   timeoutId:0,
   aurhor:'emoker',
   test: function () {
     console.log("Toast from "+this.aurhor);
     // console.log('{"ele":"'+this.ele+'","icon":"'+this.icon+'","text":"'+this.text+'","duration":"'+this.duration+'","timeoutId":"'+this.timeoutId+'","aurhor":"'+this.aurhor+'"}')
   },
   init:function(ele){
     this.ele = ele;
     if ($('.toast').length == 0 && $('#toast').length == 0) {
       $('body').append(this.eleHtml);
     }
     console.log('Toast initial complete.');
   },
   set:function (t,i,d) {
     var icon = i || this.icon;
     var text = t|| this.text;
     var duration = d|| this.duration;
     var ele = this.ele || "toast";
     ele = "#"+ele;
     if($('.toast').length == 0 && $('#toast').length == 0){
       this.init();
     }
     this.clear();
     $(ele).find('>.toast-icon').addClass(icon);
     $(ele).find('>.toast-text').html(text);
     var _this = this;
     this.timeoutId = setTimeout(function () { _this.hide(); },duration);
   },
   show:function (t,i,d) {
     this.set(t,i,d);
     var ele = this.ele || "toast";
     ele = "#"+ele;
     $(ele).addClass('t-active');
   },
   hide: function () {
     var ele = this.ele || "toast";
     ele = "#"+ele;
     clearTimeout(this.timeoutId);
     $(ele).removeClass('t-active');
   },
   clear: function () {
     var ele = this.ele || "toast";
     ele = "#"+ele;
     $(ele).find('>.toast-icon').removeClass("success");
     $(ele).find('>.toast-icon').removeClass('error');
     $(ele).find('>.toast-icon').removeClass('load');
     $(ele).find('>.toast-text').html('');
     clearTimeout(this.timeoutId);
   }
 };

 /* toast end */
