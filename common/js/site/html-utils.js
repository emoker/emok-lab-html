/**
 * 
 */
//HTML转义
function HTMLEncode(html) {
	var temp = document.createElement("div");
	(temp.textContent != null) ? (temp.textContent = html)
			: (temp.innerText = html);
	var output = temp.innerHTML;
	temp = null;
	return output;
}

//HTML反转义
function HTMLDecode(text) {
	var temp = document.createElement("div");
	temp.innerHTML = text;
	var output = temp.innerText || temp.textContent;
	temp = null;
	return output;
}