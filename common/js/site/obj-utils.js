/**
 * obj的属性值空值处理工具
 */

/*
复杂对象的值的判断与处理
*/
function ObjNullValueReplace(obj,replace) {
  if (typeof(obj) != 'object') {
    return NullValueReplace(obj,replace);
  }
  for(var i in obj){
    obj[i] = NullValueReplace(obj[i],replace);
  }
  return obj
}
/*
简单类型的值的判断与处理
*/
function NullValueReplace(val,replace) {
  if (isNullOrUndefined(val)) {
    return replace;
  }
  return val;
}
function isNullOrUndefined(exp) {
  if (exp === 0) {
    return false;
  }
  // if (!exp && typeof(exp)!='undefined' )
  if (!exp )
  {
    return true;
  }else {
    return false;
  }
}