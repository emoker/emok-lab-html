/**
 * 转换数字和汉字的处理工具
 */
var transNumberUtils = (function (option) {
	var chnNumChar = ["零","一","二","三","四","五","六","七","八","九"];
	var chnUnitSection = ["","万","亿","万亿","亿亿"];
	var chnUnitChar = ["","十","百","千"];
	var chnNumCharArray = [{
		"零":0,
		"一":1,
		"二":2,
		"三":3,
		"四":4,
		"五":5,
		"六":6,
		"七":7,
		"八":8,
		"九":9
	}];
	var chnNameValue = {
			十:{value:10, secUnit:false},
			百:{value:100, secUnit:false},
			千:{value:1000, secUnit:false},
			万:{value:10000, secUnit:true},
			亿:{value:100000000, secUnit:true}
	}
	var numToChn = function(num){
		var index =  num.toString().indexOf(".");
		if(index != -1){
			var str = num.toString().slice(index);
			var a = "点";
			for(var i=1;i<str.length;i++){
				a += chnNumChar[parseInt(str[i])];
			}
			return a ;
		}else{
			return ;
		}
	}
	return {
		//定义在每个小节的内部进行转化的方法，其他部分则与小节内部转化方法相同
		sectionToChinese: function (section){
			var str = '', chnstr = '',zero= false,count=0;   //zero为是否进行补零， 第一次进行取余由于为个位数，默认不补零
			while(section>0){
				var v = section % 10;  //对数字取余10，得到的数即为个位数
				if(v ==0){                    //如果数字为零，则对字符串进行补零
					if(zero){
						zero = false;        //如果遇到连续多次取余都是0，那么只需补一个零即可
						chnstr = chnNumChar[v] + chnstr; 
					}      
				}else{
					zero = true;           //第一次取余之后，如果再次取余为零，则需要补零
					str = chnNumChar[v];
					str += chnUnitChar[count];
					chnstr = str + chnstr;
				}
				count++;
				section = Math.floor(section/10);
			}
			return chnstr;
		},
		//定义整个数字全部转换的方法，需要依次对数字进行10000为单位的取余，然后分成小节，按小节计算，当每个小节的数不足1000时，则需要进行补零
		TransformToChinese: function (num){
			var tenUnit = num < 20 && num > 9;
			var a = numToChn(num);
			num = Math.floor(num);
			var unitPos = 0;
			var strIns = '', chnStr = '';
			var needZero = false;

			if(num === 0){
				return chnNumChar[0];
			} 
			while(num > 0){
				var section = num % 10000;
				if(needZero){
					chnStr = chnNumChar[0] + chnStr;
				}
				strIns = this.sectionToChinese(section);
				strIns += (section !== 0) ? chnUnitSection[unitPos] : chnUnitSection[0];
				chnStr = strIns + chnStr;
				needZero = (section < 1000) && (section > 0);
				num = Math.floor(num / 10000);
				unitPos++;
			}
			chnStr = a?chnStr+a:chnStr;
			if (tenUnit) {
				chnStrArr = chnStr.split(chnUnitChar[1]);
				chnStr = chnStrArr.length > 1 ? chnUnitChar[1]+chnStrArr[1] : chnStr;
			}
			return chnStr;
		},
		ChineseToNumber: function (chnStr){
			var rtn = 0;
			var section = 0;
			var number = 0;
			var secUnit = false;
			var str = chnStr.split('');

			for(var i = 0; i < str.length; i++){
				var num = chnNumCharArray[0][str[i]];
				if(typeof num !== 'undefined'){
					number = num;
					if(i === str.length - 1){
						section += number;
					}
				}else{
					var unit = chnNameValue[str[i]].value;
					secUnit = chnNameValue[str[i]].secUnit;
					if(secUnit){
						section = (section + number) * unit;
						rtn += section;
						section = 0;
					}else{
						section += (number * unit);
					}
					number = 0;
				}
			}
			return rtn + section;
		}
	}
})();