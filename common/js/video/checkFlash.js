/*
 * 编写:张志鑫
 * 检查浏览器是否有安装Flash 或者允不允许该网站在浏览器加载Flash插件
 * 
 */
function IsPC() {
    var userAgentInfo = navigator.userAgent;
    var Agents = ["Android", "iPhone",
                "SymbianOS", "Windows Phone",
                "iPad", "iPod"];
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
        if (userAgentInfo.indexOf(Agents[v]) > 0) {
            flag = false;
            break;
        }
    }
    return flag;
}
 function hasFlash() {
	 var swf;
    if (navigator.userAgent.indexOf("MSIE") > 0) {
        try {
            var swf = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
            alert('已安装flash插件......');
            return alreadyFlash();
        }
        catch (e) {
            alert('没有安装flash插件...');
        	return generInstallFlash();
        }
    }
    if (navigator.userAgent.indexOf("Firefox") > 0 || navigator.userAgent.indexOf("Chrome") > 0) {
        swf = navigator.plugins["Shockwave Flash"];
        return (swf) ? alreadyFlash() : generInstallFlash();
    }
  }
function alreadyFlash(){
	$("#xiufuflash").html("");
    return true;
}
function generInstallFlash(){
	$("#xiufuflash").removeAttr("disabled");
   	return false;
}