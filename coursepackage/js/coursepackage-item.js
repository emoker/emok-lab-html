/**
 * 对指定的obj增加属性val
 **/
function defineReactive(obj, key, val, callback) {
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: false,
    get: function () {
      return val;
    },
    set: function (newVal) {
      //通知回调函数处理数据
      var rawVal = val;
      val = newVal;
      callback && callback.call(obj, newVal, rawVal);
      rawVal = null;
    }
  });
}
function CoursePackageList() {
  var coursePackages = [];
  var htmlTemp = 'temp';
}
CoursePackageList.prototype = {
  test: function (test) {
    console.log('test. ', test, ' htmlTemp', this.htmlTemp);
  },
  init: function (option) {
    console.log('CoursePackageList init.', option ? option : 'no prarm');
    var self = this;
    defineReactive(self, 'coursePackages', [], self.generateCoursePackageHtml)
    self.coursePackages = option || self.coursePackages;
  },
  getCoursePackagesDate: function () {
    console.log('getCoursePackagesDate. ');
    // $.ajax({
    //   url = "http://127.0.0.1" + '/synchronizing/getAllCoursePackage',
    //   type: 'post',
    //   dataType: 'jsonp',

    // })
  },
  generateCoursePackageHtml: function (coursePackages) {
    console.log('generateCoursePackageHtml. ', coursePackages);
    var html = '';
    html += '<div class="col-md-4 pa-10">';
    html += '    <div class="coursepackage-item sxbang-bg-white" onclick="location.href=\'{{coursepackagePageUrl}}\'">';
    html += '        <div class="coursepackage-item-cover">';
    html += '            <img src="{{coursepackageCover}}" alt="">';
    html += '            <div class="coursepackage-item-cover-tag px-20 py-5 sxbang-bg-green sxbang-color-white sxbang-font-16">';
    html += '                {{coursepackageLocation}}';
    html += '            </div>';
    html += '        </div>';
    html += '        <div class="pa-30">';
    html += '            <div class="coursepackage-item-title sxbang-font-18 pb-5">';
    html += '                <div';
    html += '                    class="coursepackage-item-status status-{{coursepackageStatus}} sxbang-round-btn px-10 py-5 sxbang-color-green sxbang-font-14 mr-10">';
    html += '                    {{coursepackageStatusText}}</div>';
    html += '                <span class="fw-bold">{{coursepackageTitle}}</span>';
    html += '            </div>';
    html += '            <div class="coursepackage-item-desc pb-10">';
    html += '                {{coursepackageDesc}}';
    html += '            </div>';
    html += '            <div class="coursepackage-item-tag-tech sxbang-round-btn px-15 py-5 sxbang-color-green mb-15">';
    html += '                {{coursepackageTechTag}}';
    html += '            </div>';
    html += '            <div class="coursepackage-item-info pb-15">';
    html += '                <div class="info-tag">';
    html += '                    <img class="coursepackage-item-info-icon" src="./coursepackage/img/SVG/icon_location.svg" alt="">';
    html += '                    <span class="tag-location mr-15">{{coursepackageLocation}}</span>';
    html += '                    <img class="coursepackage-item-info-icon" src="./coursepackage/img/SVG/icon_time.svg" alt="">';
    html += '                    <span class="tag-time">{{coursepackageStartDate}}</span>';
    html += '                </div>';
    html += '            </div>';
    html += '            <div';
    html += '                class="coursepackage-item-more py-10 ta-center sxbang-font-18 sxbang-bg-green sxbang-color-white sxbang-round-btn">';
    html += '                查看更多';
    html += '            </div>';
    html += '        </div>';
    html += '    </div>';
    html += '</div>';
    var now = new Date();
    coursePackages.forEach(function (item, index) {
      // console.log('coursePackages.forEach ',item)
      var temp = html;
      var classStartDate = new Date(item.classStartDate);
      var classEndDate = new Date(item.classEndDate);
      var statusText = ['未开始','进行中','已结束'];
      var status = now < classStartDate ? 0:  now > classEndDate ? 2 : 1;
      temp = temp.replace(/{{coursepackageCover}}/g, item.classPicture);
      temp = temp.replace(/{{coursepackageLocation}}/g, '珠海');
      temp = temp.replace(/{{coursepackageStatus}}/g, status);
      temp = temp.replace(/{{coursepackageStatusText}}/g, statusText[status]);
      temp = temp.replace(/{{coursepackageTitle}}/g, item.coursePackageName);
      temp = temp.replace(/{{coursepackageDesc}}/g, item.description);
      temp = temp.replace(/{{coursepackageTechTag}}/g, '性能测试、接口测试、自动化测试等');
      temp = temp.replace(/{{coursepackageStartDate}}/g, item.classStartDate.replace(/-/g,'.').replace(/\//g,'.'));
      temp = temp.replace(/{{coursepackagePageUrl}}/g, '/java201905');
      $('#coursepackageList').find('.container-bd').append(temp);
    });
  }
}
var coursePackageList = new CoursePackageList();
$(function () {
  coursePackageList.test('tt')
  coursePackageList.init(coursePackagesDate)
});

var coursePackagesDate = [
  {
    "id": 10,
    "coursePackageName": "金山网游测试训练营（6期）",
    "classPicture": 'http://file.sxbang.net/coursepackage/index/test_traning_3-over.jpg',
    "page": '',
    "description": '全栈测试生涯从游戏开始，web手工测试，app手工测试，接口手工测试，I自动化测试，接口自动化测试，性能测试等职位。',
    "technologyLabel": null,
    "state": null,
    "classStartDate": '2019/05/05',
    "classEndDate": '2019/05/30',
    "startDate": null,
    "endDate": null,
    "originPrice": null,
    "favorabledPrice": null,
    "createDatetime": null,
    "coursePackageComment": null,
    "isShow": null,
    "isShowSort": null,
    "openClass": null,
    "teacherId": null,
    "personPicture": null,
    "viewNumber": null,
    "personDescription": null,
    "finallPrice": null,
    "couponPrice": null
  },
  {
    "id": 9,
    "coursePackageName": "Springboot+Vue高并发秒杀场景",
    "classPicture": 'http://file.sxbang.net/coursepackage/index/java_traning_1-over.jpg',
    "page": null,
    "description": '全栈测试生涯从游戏开始，web手工测试，app手工测试，接口手工测试，I自动化测试，接口自动化测试，性能测试等职位。',
    "technologyLabel": null,
    "state": null,
    "classStartDate": '2019/05/05',
    "classEndDate": '2019/05/30',
    "startDate": null,
    "endDate": null,
    "originPrice": null,
    "favorabledPrice": null,
    "createDatetime": null,
    "coursePackageComment": null,
    "isShow": null,
    "isShowSort": null,
    "openClass": null,
    "teacherId": null,
    "personPicture": null,
    "viewNumber": null,
    "personDescription": null,
    "finallPrice": null,
    "couponPrice": null
  },
  {
    "id": 1,
    "coursePackageName": "金山网游测试训练营（4期）",
    "classPicture": 'http://file.sxbang.net/coursepackage/index/test_traning_1-over.jpg',
    "page": null,
    "description": '全栈测试生涯从游戏开始，web手工测试，app手工测试，接口手工测试，I自动化测试，接口自动化测试，性能测试等职位。',
    "technologyLabel": null,
    "state": null,
    "classStartDate": '2019/05/05',
    "classEndDate": '2019/05/30',
    "startDate": null,
    "endDate": null,
    "originPrice": null,
    "favorabledPrice": null,
    "createDatetime": null,
    "coursePackageComment": null,
    "isShow": null,
    "isShowSort": null,
    "openClass": null,
    "teacherId": null,
    "personPicture": null,
    "viewNumber": null,
    "personDescription": null,
    "finallPrice": null,
    "couponPrice": null
  },
  {
    "id": 11,
    "coursePackageName": "Java开发训练营（武汉）",
    "classPicture": 'http://file.sxbang.net/coursepackage/index/java_traning_2-nobegin.jpg',
    "page": null,
    "description": '全栈测试生涯从游戏开始，web手工测试，app手工测试，接口手工测试，I自动化测试，接口自动化测试，性能测试等职位。',
    "technologyLabel": null,
    "state": null,
    "classStartDate": '2019/05/05',
    "classEndDate": '2019/05/30',
    "startDate": null,
    "endDate": null,
    "originPrice": null,
    "favorabledPrice": null,
    "createDatetime": null,
    "coursePackageComment": null,
    "isShow": null,
    "isShowSort": null,
    "openClass": null,
    "teacherId": null,
    "personPicture": null,
    "viewNumber": null,
    "personDescription": null,
    "finallPrice": null,
    "couponPrice": null
  }
];